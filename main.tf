provider "aws" {
  region                 = "${var.region}"
}
# ec2 instance
resource "aws_instance" "lab_instance" {
  ami                    = "${var.ami}"
  instance_type          = "${var.ec2_instance}"
  vpc_security_group_ids = [aws_security_group.lab_instance_ingress.id]
  availability_zone      = "${var.ec2_av_zone}"
  key_name               = "${var.ec2_key}"
  
  connection {
    type                 = "ssh"
    host                 = aws_instance.lab_instance.public_ip
    user                 = var.ssh_user
    port                 = var.ssh_port
    private_key          = "${file("/home/alex/.ssh/test.pem")}"
    agent                = true
  }

  provisioner "file" {
    source               = "conf/script.sh"
    destination          = "/tmp/script.sh"
  }  
  
  provisioner "file" {
    source               = "conf/httpd.conf"
    destination          = "/tmp/httpd.conf"
  }  

  provisioner "file" {
    source               = "conf/backend.vhost.conf"
    destination          = "/tmp/backend.vhost.conf"  
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /tmp/script.sh",
      "sudo /tmp/script.sh"
    ]
  }
}