#! /bin/bash
yum -y update
yum -y install httpd
systemctl start httpd
systemctl enable httpd
yum -y install java-1.8.0-openjdk-devel
yum -y install tomcat 
systemctl start tomcat 
systemctl enable tomcat
wget -P /var/lib/tomcat/webapps https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war
cp /tmp/httpd.conf /etc/httpd/conf/httpd.conf
mkdir /etc/httpd/vhosts/
cp /tmp/backend.vhost.conf /etc/httpd/vhosts/
systemctl restart httpd