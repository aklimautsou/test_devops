# ec2 vars output
output "instance_ip_address" {
  value       = "${aws_instance.lab_instance.public_ip}"
  description = "Address of the instance"
}
output "instance_DNS_address" {
  value       = "${aws_instance.lab_instance.public_dns}"
  description = "DNS address of the instance"
}