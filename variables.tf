## aws vars
variable "region" {
  description = "aws region"
  type        = string
}
##
## ec2 vars
variable "ami" {
  description = "ID of AMI to use for the instance"
  type        = string
}
variable "ec2_instance" {
  description = "ec2 instance class"
  type        = string    
}
variable "ec2_av_zone" {
  description = "ec2 availability zone"
  type        = string    
}
variable "ec2_key" {
  description = "ec2 key name"
  type        = string   
}
variable "ssh_user" {
  description = "SSH user name to use for remote exec connections,"
  type        = string
}
variable "ssh_port" {
  description = "The port the EC2 Instance should listen on for SSH requests."
  type        = number
}